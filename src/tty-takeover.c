/**
 * tty-takeover - Written by Jeremy DeJournett in February 2018
 * This utility is used to take over stdin, forwarding all data
 * over UDP to the port specified as an argument. It then reads the
 * UDP response and prints it to stdout. This can be used to take
 * over the debug port of the satellite and communicate directly
 * with a running daemon.
 *
 * ======================
 *   !!  ATTENTION  !!
 * ======================
 * You will probably want to run in -x, or hex mode, which allows you
 * to send ASCII hexadecimal strings that end in '\n' in order to send
 * a command, and which converts any response from raw binary to a
 * hexadecimal string which the listener can then convert to binary as
 * needed. We have to do this because on most terminals, it doesn't
 * actually send to stdin until you get a newline character. Encoding in
 * ASCII hex circumvents this issue
 */
#include "libdebug.h"
#include "libudp.h"
#include "libtty.h"
#include "libhmt.h"
#include "librpc.h"

void parseOptions(int argc, char ** argv);
int usage(void);

int sockfd = -1;
uint16_t listen_port = 5716, send_port = 5717;
int hex_mode = 0;
int verbose = 0;
int nonblocking = 0;
char *program_name = NULL;

#define STRINGIFY(x) #x

#define VPRINT(...)                 \
    do {                            \
        if(verbose)                 \
            printf(__VA_ARGS__);    \
    } while(0)


int usage(void)
{
    fprintf(stderr, "%s [-l listen_port] [-s send_port] [-x (enable hex mode)] [-n (use nonblocking UDP on listen_port]\n"
                    , program_name);
    return EXIT_FAILURE;
}

void parseOptions(int argc, char ** argv)
{
    int c;
    uint16_t temp;

#define SET_MSG(var)        \
    if(parseUnsignedShort(optarg, &temp)) {\
        exit(usage());\
    } else {\
        var = temp;\
    }
    //    printf("set %s to %"PRIu16"\n", #var, var);
    while((c = getopt(argc, argv, "l:s:hxvn")) != -1) {
        switch(c) {
        case 'l':
            SET_MSG(listen_port);
            break;
        case 's':
            SET_MSG(send_port);
            break;
        case 'h':
            exit(usage());
            break;
        case 'x':
            hex_mode = 1;
            break;
        case 'v':
            verbose = 1;
            break;
        case 'n':
            nonblocking = 1;
            break;
        default:
            fprintf(stderr, "unrecognized option %c\n", optopt);
            exit(usage());
            break;
        }
    }
#undef SET_MSG
}

int main(int argc, char ** argv)
{
    uint8_t stdin_buf[1<<16];
    uint8_t udp_send_buf[1<<16];
    uint8_t stdout_buf[1<<16];
    size_t nread;
    program_name = argv[0];
    parseOptions(argc, argv);

    printf("listening on udp:%"PRIu16"\n", listen_port);
    printf("forwarding stdin to udp:%"PRIu16"\n", send_port);

    int (*setup_function)(int*, uint16_t) = NULL;

    setup_function = nonblocking ? setupUdpSocketN : setupUdpSocketB;

    if(setup_function(&sockfd, listen_port)) {
        perror("unable to setup UDP socket");
        exit(usage());
    }
    while(1)
    {
        ssize_t r;
        if((r = read(STDIN_FILENO, stdin_buf, sizeof(stdin_buf))) < 0) {
            exit(EXIT_FAILURE); // TODO: send some junk to let em know its down?
        }
        nread = (size_t)r;
        if(hex_mode) {
            stdin_buf[nread] = '\0';
            removeChar((char*)stdin_buf, '\n');
            fillHexBufFromString(udp_send_buf, (const char *)stdin_buf, nread);
            nread = strlen((char *) stdin_buf)/2;
        } else {
            memcpy(udp_send_buf, stdin_buf, nread);
        }
        size_t out_size = sizeof(stdout_buf);
        memset(stdout_buf, 0, 8); // clear 8 bytes
        if(rpc_recv_buf_varlen(send_port, *(uint16_t*)udp_send_buf, stdout_buf + 8, &out_size, 10*1000*1000)) {
            continue;
        }
        out_size += 8;
        if(hex_mode) {
            logDataAsHex(stdout, stdout_buf, out_size);
            logBuf( STRINGIFY(stdout_buf), stdout_buf, out_size, __func__, __LINE__);
        } else {
            write(STDOUT_FILENO, stdin_buf, out_size);
        }
    }

    return EXIT_SUCCESS;
}
